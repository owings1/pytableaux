# Third-Party Notices


<!--
-------------------------------------------------------------------------------

<a name="#"></a>

### ... (MIT)

  <details><summary>Details</summary>

  #### Origin

  - **Project**   - [            ][-git]
  - **Author**    - [            ][-who]
  - **License**   - [MIT License ][-lic]
  - **Version**   - [            ][-tre]
  - **Published** - 
  - **Language**  - `JavaScript`

  #### Usage

  - **Added** - 
  - **Purpose** - Library
  - **Linkage** - Repackaged (partial, minified)

  #### Paths

  - [/pytableaux/]()

  </details>

  <details><summary>License</summary>

  ```

  ```
  </details>

[-git]: https://github.com/
[-who]: https://github.com/
[-tre]: https://github.com/
[-lic]: https://github.com/

-------------------------------------------------------------------------------
-->
| Component                  | Version                | License             |
|:---------------------------|------------------------|---------------------|
| [font-awesome font](#e4bb) | [ 4.7.0    ][e4bb-tre] | [OFL 1.1][e4bb-lic] |
| [charmonman font  ](#7aa0) | [ 8590fc4a ][7aa0-tre] | [OFL 1.1][7aa0-lic] |
| [jquery      ](#8fb8)      | [ 3.6.0    ][8fb8-tre] | [MIT    ][8fb8-lic] |
| [jquery-ui   ](#4e0f)      | [ 1.12.1   ][4e0f-tre] | [MIT    ][4e0f-lic] |
| [json-viewer ](#2cdb)      | [ 2cdbde04 ][2cdb-tre] | [MIT    ][2cdb-lic] |
<!--
| [ ](#)  | [  ][-tre] | [MIT][-lic]  |
| [ ](#)  | [  ][-tre] | [MIT][-lic]  |
| [ ](#)  | [  ][-tre] | [MIT][-lic]  |
-->

-------------------------------------------------------------------------------

<a name="#e4bb"></a>

### font-awesome font 4.7.0 (OFL 1.1)

  <details><summary>Details</summary>

  #### Origin

  - **Project**   - [Font Awesome][e4bb-git]
  - **Author**    - [Dave Gandy][e4bb-who]
  - **License**   - [SIL Open Font License, Version 1.1][e4bb-lic]
  - **Version**   - [4.7.0][e4bb-tre]
  - **Published** - 2016-10-24
  - **Language**  - `opentype` `TTF`

  #### Usage

  - **Added**   - 2021-10-06
  - **Purpose** - Web Font
  - **Linkage** - Repackaged (partial)

  #### Paths

  - [/pytableaux/web/static/css/fonts/fontawesome](/pytableaux/web/static/css/fonts/fontawesome)

  </details>

  <details><summary>License</summary>

  ```
  Copyright (c) 2012-2016, Dave Gandy (<dave@fontawesome.io>).

  This Font Software is licensed under the SIL Open Font License, Version 1.1.

  This license is copied below, and is also available with a FAQ at: https://scripts.sil.org/OFL

  SIL OPEN FONT LICENSE
  Version 1.1 - 26 February 2007

  PREAMBLE
  The goals of the Open Font License (OFL) are to stimulate worldwide
  development of collaborative font projects, to support the font creation
  efforts of academic and linguistic communities, and to provide a free and
  open framework in which fonts may be shared and improved in partnership
  with others.

  The OFL allows the licensed fonts to be used, studied, modified and
  redistributed freely as long as they are not sold by themselves. The
  fonts, including any derivative works, can be bundled, embedded,
  redistributed and/or sold with any software provided that any reserved
  names are not used by derivative works. The fonts and derivatives,
  however, cannot be released under any other type of license. The
  requirement for fonts to remain under this license does not apply
  to any document created using the fonts or their derivatives.

  DEFINITIONS
  "Font Software" refers to the set of files released by the Copyright
  Holder(s) under this license and clearly marked as such. This may
  include source files, build scripts and documentation.

  "Reserved Font Name" refers to any names specified as such after the
  copyright statement(s).

  "Original Version" refers to the collection of Font Software components as
  distributed by the Copyright Holder(s).

  "Modified Version" refers to any derivative made by adding to, deleting,
  or substituting — in part or in whole — any of the components of the
  Original Version, by changing formats or by porting the Font Software to a
  new environment.

  "Author" refers to any designer, engineer, programmer, technical
  writer or other person who contributed to the Font Software.

  PERMISSION & CONDITIONS
  Permission is hereby granted, free of charge, to any person obtaining
  a copy of the Font Software, to use, study, copy, merge, embed, modify,
  redistribute, and sell modified and unmodified copies of the Font
  Software, subject to the following conditions:

  1) Neither the Font Software nor any of its individual components,
  in Original or Modified Versions, may be sold by itself.

  2) Original or Modified Versions of the Font Software may be bundled,
  redistributed and/or sold with any software, provided that each copy
  contains the above copyright notice and this license. These can be
  included either as stand-alone text files, human-readable headers or
  in the appropriate machine-readable metadata fields within text or
  binary files as long as those fields can be easily viewed by the user.

  3) No Modified Version of the Font Software may use the Reserved Font
  Name(s) unless explicit written permission is granted by the corresponding
  Copyright Holder. This restriction only applies to the primary font name as
  presented to the users.

  4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
  Software shall not be used to promote, endorse or advertise any
  Modified Version, except to acknowledge the contribution(s) of the
  Copyright Holder(s) and the Author(s) or with their explicit written
  permission.

  5) The Font Software, modified or unmodified, in part or in whole,
  must be distributed entirely under this license, and must not be
  distributed under any other license. The requirement for fonts to
  remain under this license does not apply to any document created
  using the Font Software.

  TERMINATION
  This license becomes null and void if any of the above conditions are
  not met.

  DISCLAIMER
  THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
  OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
  DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
  OTHER DEALINGS IN THE FONT SOFTWARE.
  ```
  </details>

[e4bb-git]: https://github.com/FortAwesome/Font-Awesome
[e4bb-who]: https://github.com/davegandy
[e4bb-tre]: https://github.com/FortAwesome/Font-Awesome/tree/v4.7.0
[e4bb-lic]: https://fontawesome.com/license/free

-------------------------------------------------------------------------------

<a name="#7aa0"></a>

### charmonman font (OFL 1.1)

  <details><summary>Details</summary>

  #### Origin

  - **Project**   - [Charmonman][7aa0-git]
  - **Author**    - [Cadson Demak and others][7aa0-who]
  - **License**   - [SIL Open Font License, Version 1.1][7aa0-lic]
  - **Version**   - [#8590fc4a][7aa0-tre]
  - **Published** - 2018-08-20
  - **Language**  - `glyphs` `TTF`

  #### Usage

  - **Added** - 2020-05-21
  - **Update** - 2022-04-17
  - **Purpose** - Web Font
  - **Linkage** - Repackaged (partial)

  #### Paths

  - [/pytableaux/web/static/css/fonts/charmonman](/pytableaux/web/static/css/fonts/charmonman)

  </details>

  <details><summary>License</summary>

  ```
  Copyright 2018 The Charmonman Project Authors (https://github.com/cadsondemak/Charmonman)

  This Font Software is licensed under the SIL Open Font License, Version 1.1.
  This license is copied below, and is also available with a FAQ at:
  http://scripts.sil.org/OFL

  -----------------------------------------------------------
  SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007
  -----------------------------------------------------------

  PREAMBLE
  The goals of the Open Font License (OFL) are to stimulate worldwide
  development of collaborative font projects, to support the font creation
  efforts of academic and linguistic communities, and to provide a free and
  open framework in which fonts may be shared and improved in partnership
  with others.

  The OFL allows the licensed fonts to be used, studied, modified and
  redistributed freely as long as they are not sold by themselves. The
  fonts, including any derivative works, can be bundled, embedded, 
  redistributed and/or sold with any software provided that any reserved
  names are not used by derivative works. The fonts and derivatives,
  however, cannot be released under any other type of license. The
  requirement for fonts to remain under this license does not apply
  to any document created using the fonts or their derivatives.

  DEFINITIONS
  "Font Software" refers to the set of files released by the Copyright
  Holder(s) under this license and clearly marked as such. This may
  include source files, build scripts and documentation.

  "Reserved Font Name" refers to any names specified as such after the
  copyright statement(s).

  "Original Version" refers to the collection of Font Software components as
  distributed by the Copyright Holder(s).

  "Modified Version" refers to any derivative made by adding to, deleting,
  or substituting -- in part or in whole -- any of the components of the
  Original Version, by changing formats or by porting the Font Software to a
  new environment.

  "Author" refers to any designer, engineer, programmer, technical
  writer or other person who contributed to the Font Software.

  PERMISSION & CONDITIONS
  Permission is hereby granted, free of charge, to any person obtaining
  a copy of the Font Software, to use, study, copy, merge, embed, modify,
  redistribute, and sell modified and unmodified copies of the Font
  Software, subject to the following conditions:

  1) Neither the Font Software nor any of its individual components,
  in Original or Modified Versions, may be sold by itself.

  2) Original or Modified Versions of the Font Software may be bundled,
  redistributed and/or sold with any software, provided that each copy
  contains the above copyright notice and this license. These can be
  included either as stand-alone text files, human-readable headers or
  in the appropriate machine-readable metadata fields within text or
  binary files as long as those fields can be easily viewed by the user.

  3) No Modified Version of the Font Software may use the Reserved Font
  Name(s) unless explicit written permission is granted by the corresponding
  Copyright Holder. This restriction only applies to the primary font name as
  presented to the users.

  4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
  Software shall not be used to promote, endorse or advertise any
  Modified Version, except to acknowledge the contribution(s) of the
  Copyright Holder(s) and the Author(s) or with their explicit written
  permission.

  5) The Font Software, modified or unmodified, in part or in whole,
  must be distributed entirely under this license, and must not be
  distributed under any other license. The requirement for fonts to
  remain under this license does not apply to any document created
  using the Font Software.

  TERMINATION
  This license becomes null and void if any of the above conditions are
  not met.

  DISCLAIMER
  THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
  OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
  DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
  OTHER DEALINGS IN THE FONT SOFTWARE.
  ```
  </details>

[7aa0-git]: https://github.com/cadsondemak/Charmonman
[7aa0-who]: https://github.com/cadsondemak/Charmonman/raw/8590fc4a/AUTHORS.txt
[7aa0-tre]: https://github.com/cadsondemak/Charmonman/tree/8590fc4a
[7aa0-lic]: https://github.com/cadsondemak/Charmonman/raw/8590fc4a/OFL.txt

-------------------------------------------------------------------------------

<a name="8fb8"></a>

### jquery 3.6.0 (MIT)

  <details><summary>Details</summary>

  #### Origin

  - **Project**   - [jQuery JavaScript Library   ][8fb8-git]
  - **Author**    - [OpenJS Foundation and others][8fb8-who]
  - **License**   - [MIT License                 ][8fb8-lic]
  - **Version**   - [v3.6.0                      ][8fb8-tre]
  - **Published** - 2021-03-02
  - **Language**  - `JavaScript`

  #### Usage

  - **Added**   - 2021-09-25
  - **Purpose** - Library
  - **Linkage** - Repackaged (partial, minified)

  #### Paths

  - [/pytableaux/web/static/js/jquery-3.6.0.min.js](/pytableaux/web/static/js/jquery-3.6.0.min.js)

  </details>

  <details><summary>License</summary>

  ```
  Copyright OpenJS Foundation and other contributors, https://openjsf.org/

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ```
  </details>

[8fb8-git]: https://github.com/jquery/jquery
[8fb8-who]: https://openjsf.org/
[8fb8-tre]: https://github.com/jquery/jquery/tree/3.6.0
[8fb8-lic]: https://github.com/jquery/jquery/raw/3.6.0/LICENSE.txt

-------------------------------------------------------------------------------

<a name="4e0f"></a>

### jquery-ui 1.12.1 (MIT)

  <details><summary>Details</summary>

  #### Origin

  - **Project**   - [jQuery UI                   ][4e0f-git]
  - **Author**    - [jQuery Foundation and others][4e0f-who]
  - **License**   - [MIT License                 ][4e0f-lic]
  - **Version**   - [v1.12.1                     ][4e0f-tre]
  - **Published** - 2016-09-14
  - **Language**  - `JavaScript` `CSS`

  #### Usage

  - **Added**   - 2020-05-13
  - **Purpose** - Library
  - **Linkage** - Repackaged (partial, minified)

  #### Paths

  - [/pytableaux/web/static/js/jquery-ui.min.js](/pytableaux/web/static/js/jquery-ui.min.js)
  - [/pytableaux/web/static/ui-base](/pytableaux/web/static/ui-base)
  - [/pytableaux/web/static/ui-controls](/pytableaux/web/static/ui-controls)

  </details>

  <details><summary>License</summary>

  ```
  Copyright jQuery Foundation and other contributors, https://jquery.org/

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ```
  </details>

[4e0f-git]: https://github.com/jquery/jquery-ui
[4e0f-who]: https://jquery.org/
[4e0f-tre]: https://github.com/jquery/jquery-ui/tree/1.12.1
[4e0f-lic]: https://github.com/jquery/jquery-ui/raw/1.12.1/LICENSE.txt

-------------------------------------------------------------------------------

<a name="2cdb"></a>

### json-viewer (MIT)

  <details><summary>Details</summary>

  #### Origin

  - **Project**   - [jQuery json-viewer][2cdb-git]
  - **Author**    - [Alexandre Bodelot ][2cdb-who]
  - **License**   - [MIT License       ][2cdb-lic]
  - **Version**   - [#2cdbde04         ][2cdb-tre]
  - **Published** - 2021-07-16
  - **Language**  - `JavaScript` `CSS`

  #### Usage

  - **Added** - 2022-03-21
  - **Purpose** - Development Tool
  - **Linkage** - Repackaged (partial)

  #### Paths

  - [/pytableaux/web/static/json-viewer](/pytableaux/web/static/json-viewer)

  #### Modifications

  - [5d52567d][5d52567d] 2022-03-21
  - [8975c62b][8975c62b] 2022-03-21

  </details>

  <details><summary>License</summary>

  ```
  The MIT License (MIT)

  Copyright (c) 2014 Alexandre Bodelot

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
  ```
  </details>

[2cdb-git]: https://github.com/abodelot/jquery.json-viewer
[2cdb-who]: https://github.com/abodelot
[2cdb-lic]: https://github.com/abodelot/jquery.json-viewer/raw/2cdbde04/LICENSE
[2cdb-tre]: https://github.com/abodelot/jquery.json-viewer/tree/2cdbde044104075b1ece6fcf6d395f327fe2f01f
[8975c62b]: https://github.com/owings1/pytableaux/commit/8975c62bde1d8ff976a393550c898801baf47169
[5d52567d]: https://github.com/owings1/pytableaux/commit/5d52567da020cd49185017c7440a78f4f1bf83a4

-------------------------------------------------------------------------------