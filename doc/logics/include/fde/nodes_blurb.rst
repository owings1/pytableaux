Nodes for tableaux include a *designation* marker: |[+]| for *designated*, and |[-]|
for *undesignated*.