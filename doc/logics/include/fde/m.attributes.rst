.. autoattribute:: designated_values

.. autoattribute:: extensions

.. autoattribute:: anti_extensions

.. autoattribute:: atomics
