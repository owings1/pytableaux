Nodes consist of either a (sentence, world) pair, indicating a true sentence at
that world, or a pair of worlds (:m:`w1`, :m:`w2`), indicating that the pair :m:`<w_1, w_2>`
is in the access relation :m:`R`.