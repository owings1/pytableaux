A necessity sentence is true at :m:`w` iff its operand is true at :m:`w'` for
each :m:`w'` such that :m:`<w, w'>` is in the access relation.