.. _glossary:

.. include:: /_inc/note-doc-stub.rsti

********
Glossary
********

.. glossary::

  conditional identity
    :s:`B` !{|-} :s:`A $ A`

  DeMorgan laws
    . . .

  conditional contraction
    :s:`A $ (A $ B)` !{conseq} :s:`A $ B`

  disjunctive syllogism
    :s:`A V B`, :s:`~B` !{|?-} :s:`A`

  exclusitivity constraint
    . . .
  
  exhaustion constraint
    . . .

  extension of
    A logic :m:`L'` is an extension of logic :m:`L` iff everything valid in
    :m:`L` is valid in :m:`L'`.

  law of excluded middle
    :s:`B` !{conseq} :s:`A V ~A`

  law of non-contradiction
    :s:`A & ~A` !{|-} :s:`B`

  logical consequence
    . . .

  logical truth
    . . . 

  truth-functional operator
    . . .

  modus ponens
    :s:`A $ B`, :s:`A` !{|-} :s:`B`

  modus tollens
    :s:`A $ B`, :s:`~B` !{|-} :s:`A`