=================================
:mod:`pytableaux.proof.common`
=================================

.. automodule:: pytableaux.proof.common

    .. autoclass:: Branch

    .. autoclass:: Node

    .. autoclass:: Target
