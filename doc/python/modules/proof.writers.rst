=================================
:mod:`pytableaux.proof.writers`
=================================


.. module:: pytableaux.proof.writers

classes
-------
    
.. autoclass:: TabWriter()
    :members:

registry
---------

.. autofunction:: register

.. autoattribute:: pytableaux.proof.writers::registry
    :no-value:
