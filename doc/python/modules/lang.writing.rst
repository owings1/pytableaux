=================================
:mod:`pytableaux.lang.writing`
=================================

.. module:: pytableaux.lang.writing

classes
-------

.. autoclass:: LexWriter
    :members: write
