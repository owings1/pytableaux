============================================
:mod:`pytableaux.lang` package
============================================

.. toctree::
    :caption: Modules
    :maxdepth: 1
    :glob:

    lang.*


.. module:: pytableaux.lang


classes
-------

.. autoclass:: Notation
    :members:

.. autoclass:: Marking
    :members:

.. autoclass:: BiCoords
    :members:

.. autoclass:: TriCoords
    :members:

.. autoclass:: TableStore
    :members:

.. autoclass:: RenderSet
    :members:
    :show-inheritance:
    :inherited-members: Mapping

