============================
:mod:`pytableaux.logics`
============================

.. module:: pytableaux.logics


registry
---------

.. autoattribute:: pytableaux.logics::registry
    :no-value:

.. autoclass:: Registry
