============================
:mod:`pytableaux.errors`
============================

.. automodule:: pytableaux.errors
    :show-inheritance:

    .. autoclass::  IllegalStateError

    .. autoclass::  RequestDataError

    .. autoclass::  ProofTimeoutError

    .. autoclass::  ParseError

    .. autoclass::  UnboundVariableError

    .. autoclass::  BoundVariableError

    .. autoclass::  MissingAttributeError

    .. autoclass::  AttributeConflictError

    .. autoclass::  DuplicateKeyError

    .. autoclass::  MissingKeyError

    .. autoclass::  DuplicateValueError

    .. autoclass::  MissingValueError

    .. autoclass::  ConfigError

    .. autoclass::  ModelValueError

    .. autoclass::  DenotationError