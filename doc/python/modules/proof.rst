============================================
:mod:`pytableaux.proof` package
============================================

.. toctree::
    :caption: Modules
    :maxdepth: 1
    :glob:

    proof.*

classes
-------

.. module:: pytableaux.proof

.. autoclass:: TableauxSystem
    :members:
