============================================
:mod:`pytableaux.tools` package
============================================

.. toctree::
    :caption: Modules
    :maxdepth: 1
    :glob:

    tools.*

.. automodule:: pytableaux.tools
    :members:
