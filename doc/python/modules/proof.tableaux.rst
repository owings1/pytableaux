=================================
:mod:`pytableaux.proof.tableaux`
=================================

.. module:: pytableaux.proof.tableaux

classes
-------

.. autoclass:: Rule
    :members:

.. autoclass:: Tableau

    .. autoattribute:: argument

    .. autoattribute:: logic

    .. autoattribute:: finished

    .. autoattribute:: valid

    .. autoattribute:: invalid

    .. autoattribute:: completed

    .. autoattribute:: premature

    .. autoattribute:: id

    .. autoattribute:: history

    .. autoattribute:: rules

    .. autoattribute:: open

    .. autoattribute:: tree

    .. automethod:: build

    .. automethod:: step

    .. automethod:: branch

    .. automethod:: add

    .. automethod:: finish

.. autoclass:: TreeStruct
    :members:

.. autoclass:: RulesRoot
    :members:

.. autoclass:: RuleGroups
    :members:

.. autoclass:: RuleGroup
    :members:
