===============================
:mod:`pytableaux.lang.collect` 
===============================

.. module:: pytableaux.lang.collect

classes
-------

.. autoclass:: Argument

.. autoclass:: Predicates
