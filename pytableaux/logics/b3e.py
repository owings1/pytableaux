# -*- coding: utf-8 -*-
# pytableaux, a multi-logic proof generator.
# Copyright (C) 2014-2023 Doug Owings.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import annotations

from pytableaux.lang import Operator as Operator
from pytableaux.proof import Branch, Node, adds, group, sdnode

from . import fde as FDE
from . import k3 as K3
from . import k3w as K3W

name = 'B3E'

class Meta(K3.Meta):
    title       = 'Bochvar 3 External Logic'
    description = 'Three-valued logic (True, False, Neither) with assertion operator'
    category_order = 50
    native_operators = FDE.Meta.native_operators + (Operator.Assertion,)

def gap(v):
    return min(v, 1 - v)

def crunch(v):
    return v - gap(v)

class Model(K3W.Model):

    def truth_function(self, oper: Operator, a, b = None, /):
        oper = Operator(oper)
        if oper is Operator.Assertion:
            return self.Value[crunch(self.Value[a].num)]
        elif oper is Operator.Conditional:
            return self.truth_function(
                Operator.Disjunction,
                self.truth_function(Operator.Negation, self.truth_function(Operator.Assertion, a)),
                self.truth_function(Operator.Assertion, b))
        elif oper is Operator.Biconditional:
            return FDE.Model.truth_function(self, oper, a, b)
        return super().truth_function(oper, a, b)

class TableauxSystem(K3.TableauxSystem):

    # operator => negated => designated
    branchables = K3W.TableauxSystem.branchables | {
        # reduction
        Operator.Conditional: ((0, 0), (0, 0)),
        # reduction
        Operator.Biconditional: ((0, 0), (0, 0)),}

@TableauxSystem.initialize
class TabRules(K3W.TabRules):
    """
    The closure rules for L{B3E} are the L{FDE} closure rule, and the {@K3} closure rule.
    The operator rules are mostly a mix of L{FDE} and {@K3W}
    rules, but with different rules for the assertion, conditional and
    biconditional operators.
    """

    class AssertionNegatedDesignated(FDE.OperatorNodeRule):
        """
        From an unticked, designated, negated assertion node *n* on a branch *b*,
        add an undesignated node to *b* with the assertion of *n*, then tick *n*.
        """
        designation = True
        negated     = True
        operator    = Operator.Assertion

        def _get_node_targets(self, node: Node, branch: Branch, /):
            s = self.sentence(node)
            # Keep designation fixed to False for inheritance below
            return adds(group(sdnode(s.lhs, False)))

    class AssertionUndesignated(AssertionNegatedDesignated):
        """
        From an unticked, undesignated assertion node *n* on a branch *b*, add
        an undesignated node to *b* with the assertion of *n*, then tick *n*.
        """
        designation = False
        negated     = False

    class AssertionNegatedUndesignated(AssertionNegatedDesignated):
        """
        From an unticked, undesignated, negated assertion node *n* on a branch *b*, add
        a designated node to *b* with the assertion of *n*, then tick *n*.
        """
        designation = False
        negated     = True

        def _get_node_targets(self, node: Node, branch: Branch, /):
            s = self.sentence(node)
            return adds(group(sdnode(s.lhs, not self.designation)))

    class ConditionalDesignated(FDE.OperatorNodeRule):
        """
        From an unticked, designated conditional node *n* on a branch *b*,
        add a designated node to *b* with a disjunction, where the
        first disjunction is the negation of the assertion of the antecedent,
        and the second disjunct is the assertion of the consequent. Then tick *n*.
        """
        designation = True
        operator    = Operator.Conditional

        def _get_node_targets(self, node: Node, branch: Branch, /):
            s = self.sentence(node)
            sn = ~s.lhs.asserted() | s.rhs.asserted()
            # keep negated neutral for inheritance below
            if self.negated:
                sn = ~sn
            # keep designation neutral for inheritance below
            return adds(group(sdnode(sn, self.designation)))

    class ConditionalNegatedDesignated(FDE.OperatorNodeRule):
        """
        From an unticked, designated negated conditional node *n* on a branch *b*,
        add a designated node with the antecedent, and an undesigntated node
        with the consequent to *b*. Then tick *n*.
        """
        designation = True
        negated     = True
        operator    = Operator.Conditional

        def _get_node_targets(self, node: Node, branch: Branch, /):
            s = self.sentence(node)
            # Keep designation fixed for inheritance below.
            return adds(
                group(sdnode(s.lhs, True), sdnode(s.rhs, False)))

    class ConditionalUndesignated(ConditionalNegatedDesignated):
        """
        From an unticked, undesignated conditional node *n* on a branch *b*,
        add a designated node with the antecedent, and an undesigntated node
        with the consequent to *b*. Then tick *n*.
        """
        designation = False
        negated     = False

    class ConditionalNegatedUndesignated(ConditionalDesignated):
        """
        From an unticked, undesignated, negated conditional node *n* on a branch *b*,
        add an undesignated node to *b* with a negated material conditional, where the
        operands are preceded by the Assertion operator, then tick *n*.
        """
        designation = False
        negated     = True

    class BiconditionalDesignated(FDE.OperatorNodeRule):
        """
        From an unticked, designated biconditional node *n* on a branch *b*, add
        two designated nodes to *b*, one with a disjunction, where the first
        disjunct is the negated asserted antecedent, and the second disjunct is
        the asserted consequent, and the other node is the same with the disjuncts
        inverted. Then tick *n*.
        """
        designation = True
        operator    = Operator.Biconditional
        branching   = 1

        def _get_node_targets(self, node: Node, branch: Branch, /):
            s = self.sentence(node)
            lhsa = s.lhs.asserted()
            rhsa = s.rhs.asserted()
            sn1 = ~lhsa | rhsa
            sn2 = ~rhsa | lhsa
            # Keep negated neutral for inheritance below.
            if self.negated:
                sn1 = ~sn1
                sn2 = ~sn2
            # Keep designation neutral for inheritance below.
            d = self.designation
            return adds(
                group(sdnode(sn1, d), sdnode(sn2, d)))

    class BiconditionalNegatedDesignated(BiconditionalDesignated):
        """
        From an unticked, designated, biconditional node *n* on a branch *b*, add two
        undesignated nodes to *b*, one with a negated disjunction, where the
        first disjunct is the negated asserted antecedent, and the second disjunct
        is the asserted consequent, and the other node is the same with the disjuncts
        inverted. Then tick *n*.
        """
        designation = True
        negated     = True

    class BiconditionalUndesignated(BiconditionalDesignated):
        """
        From an unticked, undesignated biconditional node *n* on a branch *b*, add two
        undesignated nodes to *b*, one with a disjunction, where the
        first disjunct is the negated asserted antecedent, and the second disjunct
        is the asserted consequent, and the other node is the same with the disjuncts
        inverted. Then tick *n*.
        """
        designation = False
        negated     = False

    class BiconditionalNegatedUndesignated(BiconditionalUndesignated):
        """
        From an unticked, designated, biconditional node *n* on a branch *b*, add two
        undesignated nodes to *b*, one with a negated disjunction, where the
        first disjunct is the negated asserted antecedent, and the second disjunct
        is the asserted consequent, and the other node is the same with the disjuncts
        inverted. Then tick *n*.
        """
        designation = False
        negated     = True

    rule_groups = (
        (
            FDE.TabRules.AssertionDesignated,
            AssertionUndesignated,
            AssertionNegatedDesignated,
            AssertionNegatedUndesignated,
            FDE.TabRules.ConjunctionDesignated,
            FDE.TabRules.DisjunctionNegatedDesignated,
            ConditionalNegatedDesignated,
            ConditionalUndesignated,
            FDE.TabRules.ExistentialNegatedDesignated,
            FDE.TabRules.ExistentialNegatedUndesignated,
            FDE.TabRules.UniversalNegatedDesignated,
            FDE.TabRules.UniversalNegatedUndesignated,
            FDE.TabRules.DoubleNegationDesignated,
            FDE.TabRules.DoubleNegationUndesignated,
            # reduction rules (thus, non-branching)
            K3W.TabRules.MaterialConditionalDesignated,
            K3W.TabRules.MaterialConditionalUndesignated,
            K3W.TabRules.MaterialConditionalNegatedDesignated,
            K3W.TabRules.MaterialConditionalNegatedUndesignated,
            ConditionalDesignated,
            ConditionalNegatedUndesignated,
            K3W.TabRules.MaterialBiconditionalDesignated,
            K3W.TabRules.MaterialBiconditionalUndesignated,
            K3W.TabRules.MaterialBiconditionalNegatedDesignated,
            K3W.TabRules.MaterialBiconditionalNegatedUndesignated,
            BiconditionalDesignated,
            BiconditionalUndesignated,
            BiconditionalNegatedDesignated,
            BiconditionalNegatedUndesignated,
        ),
        (
            # two-branching rules
            FDE.TabRules.ConjunctionUndesignated,
        ),
        (
            # three-branching rules
            K3W.TabRules.DisjunctionDesignated,
            K3W.TabRules.DisjunctionUndesignated,
            K3W.TabRules.ConjunctionNegatedDesignated,
            K3W.TabRules.ConjunctionNegatedUndesignated,
            # (formerly) four-branching rules
            K3W.TabRules.DisjunctionNegatedUndesignated,
        ),
        (
            FDE.TabRules.ExistentialDesignated,
            FDE.TabRules.ExistentialUndesignated,
        ),
        (
            FDE.TabRules.UniversalDesignated,
            FDE.TabRules.UniversalUndesignated,
        ),
    )
