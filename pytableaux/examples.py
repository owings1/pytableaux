# -*- coding: utf-8 -*-
# pytableaux, a multi-logic proof generator.
# Copyright (C) 2014-2023 Doug Owings.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
pytableaux.examples
^^^^^^^^^^^^^^^^^^^

Example arguments
"""

from __future__ import annotations

from types import MappingProxyType as MapProxy

from . import logics
from .errors import check
from .lang import Argument, Parser, Predicates
from .proof import Tableau
from .tools import closure

__all__ = (
    'aliases',
    'argument',
    'arguments',
    'data',
    'preds',
    'tabiter',
    'titles')

EMPTY = ()

data = MapProxy({
    'Addition'                         : (('a',), 'Aab'),
    'Affirming a Disjunct 1'           : (('Aab', 'a'), 'b'),
    'Affirming a Disjunct 2'           : (('Aab', 'a'), 'Nb'),
    'Affirming the Consequent'         : (('Cab', 'b'), 'a'),
    'Assertion Elimination 1'          : (('Ta',), 'a' ),
    'Assertion Elimination 2'          : (('NTa',), 'Na'),
    'Biconditional Elimination 1'      : (('Bab', 'a'), 'b'),
    'Biconditional Elimination 2'      : (('Bab', 'Na'), 'Nb'),
    'Biconditional Elimination 3'      : (('NBab', 'a'), 'Nb'),
    'Biconditional Identity'           : (EMPTY, 'Baa'),
    'Biconditional Introduction 1'     : (('a', 'b'), 'Bab'),
    'Biconditional Introduction 2'     : (('Na', 'Nb'), 'Bab'),
    'Biconditional Introduction 3'     : (('a', 'Nb'), 'NBab'),
    'Conditional Contraction'          : (('UaUab',), 'Uab'),
    'Conditional Contraposition 1'     : (('Uab',), 'UNbNa'),
    'Conditional Contraposition 2'     : (('UNbNa',), 'Uab'),
    'Conditional Equivalence'          : (('Uab',), 'Uba'),
    'Conditional Identity'             : (EMPTY, 'Uaa'),
    'Conditional Modus Ponens'         : (('Uab', 'a'), 'b'),
    'Conditional Modus Tollens'        : (('Uab', 'Nb'), 'Na'),
    'Conditional Pseudo Contraction'   : (EMPTY, 'UUaUabUab'),
    'Conditional Pseudo Contraposition': (EMPTY, 'BUabUNbNa'),
    'Conjunction Commutativity'        : (('Kab',), 'Kba'),
    'Conjunction Elimination'          : (('Kab',), 'a'),
    'Conjunction Introduction'         : (('a', 'b'), 'Kab'),
    'Conjunction Pseudo Commutativity' : (EMPTY, 'BKabKba'),
    'DeMorgan 1'                       : (('NAab',), 'KNaNb'),
    'DeMorgan 2'                       : (('NKab',), 'ANaNb'),
    'DeMorgan 3'                       : (('KNaNb',), 'NAab'),
    'DeMorgan 4'                       : (('ANaNb',), 'NKab'),
    'DeMorgan 5'                       : (('Aab',), 'NKNaNb'),
    'DeMorgan 6'                       : (('Kab',), 'NANaNb'),
    'DeMorgan 7'                       : (('NKNaNb',), 'Aab'),
    'DeMorgan 8'                       : (('NANaNb',), 'Kab'),
    'Denying the Antecedent'           : (('Cab', 'Na'), 'b'),
    'Disjunction Commutativity'        : (('Aab',), 'Aba'),
    'Disjunction Pseudo Commutativity' : (EMPTY, 'BAabAba'),
    'Disjunctive Syllogism'            : (('Aab', 'Nb'), 'a'),
    'Disjunctive Syllogism 2'          : (('ANab', 'Nb'), 'Na'),
    'Existential from Universal'       : (('SxFx',), 'VxFx'),
    'Existential Syllogism'            : (('VxCFxGx', 'Fn'),  'Gn'),
    'Explosion'                        : (('KaNa',), 'b'),
    'Extracting a Disjunct 1'          : (('Aab',), 'b'),
    'Extracting a Disjunct 2'          : (('AaNb',), 'Na'),
    'Extracting the Antecedent'        : (('Cab',), 'a'),
    'Extracting the Consequent'        : (('Cab',), 'b'),
    'Identity Indiscernability 1'      : (('Fm', 'Imn'), 'Fn'),
    'Identity Indiscernability 2'      : (('Fm', 'Inm'), 'Fn'),
    'Law of Excluded Middle'           : (EMPTY, 'AaNa'),
    'Law of Non-contradiction'         : (('KaNa',), 'b'),
    'Material Biconditional Elimination 1' : (('Eab', 'a'), 'b'),
    'Material Biconditional Elimination 2' : (('Eab', 'Na'), 'Nb'),
    'Material Biconditional Elimination 3' : (('NEab', 'a'),  'Nb'),
    'Material Biconditional Identity'      : (EMPTY, 'Eaa'),
    'Material Biconditional Introduction 1': (('a', 'b'), 'Eab'),
    'Material Contraction'             : (('CaCab',), 'Cab'),
    'Material Contraposition 1'        : (('Cab',), 'CNbNa'),
    'Material Contraposition 2'        : (('CNbNa',), 'Cab'),
    'Material Identity'                : (EMPTY, 'Caa'),
    'Material Modus Ponens'            : (('Cab', 'a'), 'b'),
    'Material Modus Tollens'           : (('Cab', 'Nb'), 'Na'),
    'Material Pseudo Contraction'      : (EMPTY, 'CCaCabCab'),
    'Material Pseudo Contraposition'   : (EMPTY, 'ECabCNbNa'),
    'Modal Platitude 1'                : (('Ma',), 'Ma'),
    'Modal Platitude 2'                : (('La',), 'La'),
    'Modal Platitude 3'                : (('LMa',), 'LMa'),
    'Modal Transformation 1'           : (('La',), 'NMNa'),
    'Modal Transformation 2'           : (('NMNa',), 'La'),
    'Modal Transformation 3'           : (('NLa',), 'MNa'),
    'Modal Transformation 4'           : (('MNa',), 'NLa'),
    'Necessity Distribution 1'         : (EMPTY, 'ULUabULaLb'),
    'Necessity Distribution 2'         : (('LUab',), 'ULaLb'),
    'Necessity Elimination'            : (('La',), 'a'),
    'NP Collapse 1'                    : (('LMa',), 'Ma'),
    'Possibility Addition'             : (('a',), 'Ma'),
    'Possibility Distribution'         : (('KMaMb',), 'MKab'),
    'Quantifier Interdefinability 1'   : (('VxFx',), 'NSxNFx'),
    'Quantifier Interdefinability 2'   : (('NVxFx',), 'SxNFx'),
    'Quantifier Interdefinability 3'   : (('SxFx',), 'NVxNFx'),
    'Quantifier Interdefinability 4'   : (('NSxFx',), 'VxNFx'),
    'Reflexive Inference 1'            : (EMPTY, 'CLaa'),
    'S4 Conditional Inference 1'       : (EMPTY, 'ULaLLa'),
    'S4 Conditional Inference 2'       : (('LUaMNb', 'Ma'), 'MNb'),
    'S4 Material Inference 1'          : (EMPTY, 'CLaLLa'),
    'S4 Material Inference 2'          : (('LCaMNb', 'Ma'), 'MNb'),
    'S5 Conditional Inference 1'       : (EMPTY, 'UaLMa'),
    'S5 Material Inference 1'          : (EMPTY, 'CaLMa'),
    'Self Identity 1'                  : (EMPTY, 'Imm'),
    'Self Identity 2'                  : (EMPTY, 'VxIxx'),
    'Serial Inference 1'               : (EMPTY, 'ULaMa'),
    'Serial Inference 2'               : (('La',), 'Ma'),
    'Simplification'                   : (('Kab',), 'a'),
    'Syllogism'                        : (('VxCFxGx', 'VxCGxHx'), 'VxCFxHx'),
    'Triviality 1'                     : (EMPTY, 'a'),
    'Triviality 2'                     : (('a',), 'b'),
    'Universal Predicate Syllogism'    : (('VxVyCFxFy', 'Fm'), 'Fn'),
    'Universal from Existential'       : (('SxFx',), 'VxFx'),
})

aliases = MapProxy({
    'Triviality 1': ('TRIV', 'TRIV1'),
    'Triviality 2': ('TRIV2',),
    'Law of Excluded Middle': ('LEM',),
    'Law of Non-contradiction': ('LNC',),
    'Explosion': ('EFQ',),
    'Conditional Modus Ponens': ('MP','Modus Ponens'),
    'Conditional Modus Tollens': ('MT', 'Modus Tollens'),
    'Material Modus Ponens': ('MMP',),
    'Material Modus Tollens': ('MMT',),
    'Conditional Identity': ('Identity', 'ID'),
    'Conditional Contraction': ('Contraction',),
    'Disjunctive Syllogism': ('DS',),
    'DeMorgan 1': ('DM', 'DM1', 'DEM', 'DEM1', 'DeMorgan'),
    'DeMorgan 2': ('DM2', 'DEM2',),
    'DeMorgan 3': ('DM3', 'DEM3',),
    'DeMorgan 4': ('DM4', 'DEM4',),
    'DeMorgan 5': ('DM5', 'DEM5',),
    'DeMorgan 6': ('DM6', 'DEM6',),
    'DeMorgan 7': ('DM7', 'DEM7',),
    'DeMorgan 8': ('DM8', 'DEM8',),

    'Syllogism': ('SYL', 'SYLL'),
    'Quantifier Interdefinability 1': ('Q1',),
    'Quantifier Interdefinability 2': ('Q2',),
    'Quantifier Interdefinability 3': ('Q3',),
    'Quantifier Interdefinability 4': ('Q4',),

    'Modal Transformation 1': ('Modal 1',),
    'Modal Transformation 2': ('Modal 2',),
    'Modal Transformation 3': ('Modal 3',),
    'Modal Transformation 4': ('Modal 4',),

    'Serial Inference 1': ('SER', 'SER1', 'Serial', 'Serial 1', 'D'),
    'Serial Inference 2': ('SER2', 'Serial 2',),
    'Reflexive Inference 1': ('T', 'Reflexive', 'Reflexivity'),
    'S4 Material Inference 1': ('S4', 'S41', 'Transitive', 'RT', 'Transitivity'),
    'S4 Material Inference 2': ('S42',),
    'S5 Material Inference 1': ('S5', 'S51', 'RST')})

titles = tuple(sorted(data))

preds = Predicates(((0,0,1), (1,0,1), (2,0,1)))

@closure
def argument():

    index = {}
    cache = {}

    for name in data:
        index.update({
            k.lower(): name for k in (
                name,
                name.replace(' ', ''),
                *aliases.get(name, ''),)})

    parsearg = Parser('polish', preds.copy()).argument

    def argument(key):
        if isinstance(key, Argument):
            return key
        key = check.inst(key, str)
        title = index[key.lower()]
        if title not in cache:
            premises, conclusion = data[title]
            cache[title] = parsearg(conclusion, premises, title = title)
        return cache[title]

    return argument

def arguments(*keys):
    if not len(keys):
        keys = titles
    return tuple(map(argument, keys))


def tabiter(*logics, build = True, grouparg = False, registry = logics.registry, **opts):
    if not len(logics):
        logics = tuple(registry.all())
    if grouparg:
        it = ((logic, title) for title in titles for logic in logics)
    else:
        it = ((logic, title) for logic in logics for title in titles)
    for logic, title in it:
        tab = Tableau(logic, argument(title), **opts)
        if build:
            tab.build()
        yield tab
