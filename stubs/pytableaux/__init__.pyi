from pytableaux import errors as errors, tools as tools
from pytableaux._package_info import package as package

__docformat__: str

class _Settings:
    DEBUG: bool
    ITEM_CACHE_SIZE: int
    DOC_MODE: bool
_ENV:_Settings



# # ----- errors, tools

# from pytableaux import errors, tools
# import pytableaux.tools.abcs
# import pytableaux.tools.hooks
# import pytableaux.tools.sets
# from pytableaux.tools.sets import EMPTY_SET
# import pytableaux.tools.timing
# import pytableaux.tools.hybrids
# import pytableaux.tools.linked
# import pytableaux.lang.collect
# import pytableaux.lang.writing
# import pytableaux.lang.parsing
# import pytableaux.logics
# import pytableaux.examples
# import pytableaux.models
# import pytableaux.proof
# import pytableaux.proof.common
# import pytableaux.proof.filters
# import pytableaux.proof.tableaux
# import pytableaux.proof.rules
# import pytableaux.proof.helpers
# import pytableaux.proof.writers

# __all__ = (
#     'errors',
#     'examples',
#     'logics',
#     'models',
#     'package',
#     'proof',
#     'tools',
#     'EMPTY_SET',
# )





# Union\[(.*?), (.*?)\]
# $1|$2