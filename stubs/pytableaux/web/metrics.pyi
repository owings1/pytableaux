from collections import deque
import prometheus_client.metrics as pm
import prometheus_client.metrics_core as pmc
from prometheus_client.registry import CollectorRegistry
from pytableaux.tools import abcs, MapCover
from typing import Any, Mapping, TypeVar

class HasRegistry:
    registry: CollectorRegistry
_MetricType = pmc.Metric|pm.MetricWrapperBase|HasRegistry
MetrT = TypeVar('MetrT', bound=_MetricType)
metric_defs: deque[tuple[str, tuple[type[_MetricType], str, list[str]]]]
class AppMetrics(MapCover[str, _MetricType], abcs.Abc):
    config: Mapping[str, Any]
    registry: CollectorRegistry
    def app_requests_count(*labels: str) -> pm.Counter: ...
    def proofs_completed_count(*labels: str) -> pm.Counter: ...
    def proofs_inprogress_count(*labels: str) -> pm.Gauge: ...
    def proofs_execution_time(*labels: str) -> pm.Summary: ...
    def __init__(self, config: Mapping[str, Any], registry: CollectorRegistry = ...) -> None: ...
    @staticmethod
    def _copy_metric(m: MetrT, registry: CollectorRegistry) -> MetrT:...
