import reprlib
from pytableaux.lang.lex import LexType as LexType, Lexical as Lexical, LexicalAbc as LexicalAbc, LexicalEnum as LexicalEnum, Predicate as Predicate
from pytableaux.lang.writing import LexWriter as LexWriter
from pytableaux.tools import closure as closure

class LangRepr(reprlib.Repr):
    def repr_Constant(self, obj, level): ...

class LangRepr1(LangRepr): ...
class LangRepr2(LangRepr): ...
class LangRepr3(LangRepr): ...

def pr() -> None: ...
